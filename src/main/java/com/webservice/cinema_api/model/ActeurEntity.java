package com.webservice.cinema_api.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.sql.Date;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "acteur", schema = "cinema", catalog = "")
public class ActeurEntity {
    private int noAct;
    private String nomAct;
    private String prenAct;
    private Date dateNaiss;
    private Date dateDeces;
    private List<PersonnageEntity> roles;

    @Id
    @Column(name = "NoAct", nullable = false)
    public int getNoAct() {
        return noAct;
    }

    public void setNoAct(int noAct) {
        this.noAct = noAct;
    }

    @Basic
    @Column(name = "NomAct", nullable = false, length = 20)
    public String getNomAct() {
        return nomAct;
    }

    public void setNomAct(String nomAct) {
        this.nomAct = nomAct;
    }

    @Basic
    @Column(name = "PrenAct", nullable = true, length = 20)
    public String getPrenAct() {
        return prenAct;
    }

    public void setPrenAct(String prenAct) {
        this.prenAct = prenAct;
    }

    @Basic
    @Column(name = "DateNaiss", nullable = true)
    public Date getDateNaiss() {
        return dateNaiss;
    }

    public void setDateNaiss(Date dateNaiss) {
        this.dateNaiss = dateNaiss;
    }

    @Basic
    @Column(name = "DateDeces", nullable = true)
    public Date getDateDeces() {
        return dateDeces;
    }

    public void setDateDeces(Date dateDeces) {
        this.dateDeces = dateDeces;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ActeurEntity that = (ActeurEntity) o;
        return noAct == that.noAct &&
                Objects.equals(nomAct, that.nomAct) &&
                Objects.equals(prenAct, that.prenAct) &&
                Objects.equals(dateNaiss, that.dateNaiss) &&
                Objects.equals(dateDeces, that.dateDeces) &&
                Objects.equals(roles, that.roles);
    }

    @Override
    public int hashCode() {
        return Objects.hash(noAct, nomAct, prenAct, dateNaiss, dateDeces, roles);
    }

    @JsonIgnore
    @OneToMany
    @JoinColumn(name = "NoAct", referencedColumnName = "NoAct", insertable = false, updatable = false)
    public List<PersonnageEntity> getRoles() {
        return roles;
    }

    public void setRoles(List<PersonnageEntity> roles) {
        this.roles = roles;
    }
}
