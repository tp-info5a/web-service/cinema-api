package com.webservice.cinema_api.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "realisateur", schema = "cinema", catalog = "")
public class RealisateurEntity {
    private int noRea;
    private String nomRea;
    private String prenRea;
    private List<FilmEntity> films;

    @Id
    @Column(name = "NoRea", nullable = false)
    public int getNoRea() {
        return noRea;
    }

    public void setNoRea(int noRea) {
        this.noRea = noRea;
    }

    @Basic
    @Column(name = "NomRea", nullable = false, length = 20)
    public String getNomRea() {
        return nomRea;
    }

    public void setNomRea(String nomRea) {
        this.nomRea = nomRea;
    }

    @Basic
    @Column(name = "PrenRea", nullable = false, length = 20)
    public String getPrenRea() {
        return prenRea;
    }

    public void setPrenRea(String prenRea) {
        this.prenRea = prenRea;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RealisateurEntity that = (RealisateurEntity) o;
        return noRea == that.noRea &&
                Objects.equals(nomRea, that.nomRea) &&
                Objects.equals(prenRea, that.prenRea) &&
                Objects.equals(films,that.films);
    }

    @Override
    public int hashCode() {
        return Objects.hash(noRea, nomRea, prenRea,films);
    }

    @JsonIgnore
    @OneToMany
    @JoinColumn(name = "NoRea", referencedColumnName = "NoRea", nullable = false, updatable = false, insertable = false)
    public List<FilmEntity> getFilms() {
        return films;
    }

    public void setFilms(List<FilmEntity> films) {
        this.films = films;
    }
}
