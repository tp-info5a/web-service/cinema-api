package com.webservice.cinema_api.model;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "personnage", schema = "cinema", catalog = "")
@IdClass(PersonnageEntityPK.class)
public class PersonnageEntity {
    private int noFilm;
    private int noAct;
    private String nomPers;
    private FilmEntity filmByNoFilm;
    private ActeurEntity acteurByNoAct;

    @Id
    @Column(name = "NoFilm", nullable = false)
    public int getNoFilm() {
        return noFilm;
    }

    public void setNoFilm(int noFilm) {
        this.noFilm = noFilm;
    }

    @Id
    @Column(name = "NoAct", nullable = false)
    public int getNoAct() {
        return noAct;
    }

    public void setNoAct(int noAct) {
        this.noAct = noAct;
    }

    @Basic
    @Column(name = "NomPers", nullable = false, length = 30)
    public String getNomPers() {
        return nomPers;
    }

    public void setNomPers(String nomPers) {
        this.nomPers = nomPers;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PersonnageEntity that = (PersonnageEntity) o;
        return noFilm == that.noFilm &&
                noAct == that.noAct &&
                Objects.equals(nomPers, that.nomPers);
    }

    @Override
    public int hashCode() {
        return Objects.hash(noFilm, noAct, nomPers);
    }

    @ManyToOne
    @JoinColumn(name = "NoFilm", referencedColumnName = "NoFilm", nullable = false,insertable = false, updatable = false)
    public FilmEntity getFilmByNoFilm() {
        return filmByNoFilm;
    }

    public void setFilmByNoFilm(FilmEntity filmByNoFilm) {
        this.filmByNoFilm = filmByNoFilm;
    }

    @ManyToOne
    @JoinColumn(name = "NoAct", referencedColumnName = "NoAct", nullable = false, insertable = false, updatable = false)
    public ActeurEntity getActeurByNoAct() {
        return acteurByNoAct;
    }

    public void setActeurByNoAct(ActeurEntity acteurByNoAct) {
        this.acteurByNoAct = acteurByNoAct;
    }
}
