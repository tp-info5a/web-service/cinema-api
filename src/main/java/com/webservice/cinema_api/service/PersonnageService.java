package com.webservice.cinema_api.service;

import com.webservice.cinema_api.model.PersonnageEntity;
import com.webservice.cinema_api.model.PersonnageEntityPK;
import com.webservice.cinema_api.repository.PersonnageRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PersonnageService {

    @Autowired
    private PersonnageRepository personnageRepository;

    public List<PersonnageEntity> getPersonnages() {
        return personnageRepository.findAll();
    }

    public PersonnageEntity addPersonnage(PersonnageEntity personnageEntity) {
        return personnageRepository.save(personnageEntity);
    }

    public void deletePersonnage(PersonnageEntityPK personnageEntityPK) {
        personnageRepository.deleteById(personnageEntityPK);
    }

    public PersonnageEntity updatePersonnage(PersonnageEntity personnageEntity) {
        return personnageRepository.save(personnageEntity);
    }
}
