package com.webservice.cinema_api.service;

import com.webservice.cinema_api.model.ActeurEntity;
import com.webservice.cinema_api.model.FilmEntity;
import com.webservice.cinema_api.model.PersonnageEntity;
import com.webservice.cinema_api.repository.ActeurRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class ActeurService {

    @Autowired
    private ActeurRepository acteurRepository;

    public List<ActeurEntity> getActeurs() {
        return acteurRepository.findAll();
    }

    public List<FilmEntity> getFilmsByActorId(int id) {
        List<FilmEntity> films = new ArrayList<>();
        List<PersonnageEntity> roles = new ArrayList<>();
        Optional<ActeurEntity> acteurEntity = acteurRepository.findById(id);
        if (acteurEntity.isPresent()) {
            roles = acteurEntity.get().getRoles();
        }
        for (PersonnageEntity personnageEntity : roles) {
            films.add(personnageEntity.getFilmByNoFilm());
        }
        return films;
    }

    public List<String> getRolesByActorId(int id) {
        List<String> personnages = new ArrayList<>();
        List<PersonnageEntity> roles = new ArrayList<>();
        Optional<ActeurEntity> acteurEntity = acteurRepository.findById(id);
        if (acteurEntity.isPresent()) {
            roles = acteurEntity.get().getRoles();
        }
        for (PersonnageEntity personnageEntity : roles) {
            personnages.add(personnageEntity.getNomPers());
        }
        return personnages;
    }
}
