package com.webservice.cinema_api.service;

import com.webservice.cinema_api.model.FilmEntity;
import com.webservice.cinema_api.model.RealisateurEntity;
import com.webservice.cinema_api.repository.RealisateurRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class RealisateurService {

    @Autowired
    private RealisateurRepository realisateurRepository;

    public List<FilmEntity> getFilmByIdRea(int reaId) {
        List<FilmEntity> films = new ArrayList<>();
        Optional<RealisateurEntity> realisateurEntity = realisateurRepository.findById(reaId);
        if(realisateurEntity.isPresent()) {
            films = realisateurEntity.get().getFilms();
        }
        return films;
    }

    public List<RealisateurEntity> getRealisateurs() {
        return realisateurRepository.findAll();
    }
}
