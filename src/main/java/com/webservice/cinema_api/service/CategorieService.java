package com.webservice.cinema_api.service;

import com.webservice.cinema_api.model.CategorieEntity;
import com.webservice.cinema_api.model.FilmEntity;
import com.webservice.cinema_api.repository.CategorieRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class CategorieService {

    @Autowired
    private CategorieRepository categorieRepository;

    public List<CategorieEntity> getCategories() {
        return categorieRepository.findAll();
    }

    public List<FilmEntity> getFilmsByCategorieId(String id) {
        List<FilmEntity> films = new ArrayList<>();
        Optional<CategorieEntity> categorieEntity = categorieRepository.findById(id);
        if (categorieEntity.isPresent()) {
            films = categorieEntity.get().getFilms();
        }
        return films;
    }
}
