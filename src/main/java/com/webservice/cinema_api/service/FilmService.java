package com.webservice.cinema_api.service;

import com.webservice.cinema_api.model.FilmEntity;
import com.webservice.cinema_api.repository.FilmRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FilmService {

    @Autowired
    private FilmRepository filmRepository;

    public List<FilmEntity> getFilms() {
        return filmRepository.findAll();
    }

    public FilmEntity addFilm(FilmEntity filmEntity) {
        return filmRepository.save(filmEntity);
    }

    public void deleteFilm(int idFilm) {
        filmRepository.deleteById(idFilm);
    }

    public FilmEntity updateFilm(FilmEntity filmEntity) {
        return filmRepository.save(filmEntity);
    }

}
