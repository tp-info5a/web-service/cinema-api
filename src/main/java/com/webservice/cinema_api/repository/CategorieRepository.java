package com.webservice.cinema_api.repository;

import com.webservice.cinema_api.model.CategorieEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(exported = false)
public interface CategorieRepository extends JpaRepository<CategorieEntity, String> {

}
