package com.webservice.cinema_api.repository;

import com.webservice.cinema_api.model.PersonnageEntity;
import com.webservice.cinema_api.model.PersonnageEntityPK;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(exported = false)
public interface PersonnageRepository extends JpaRepository<PersonnageEntity, PersonnageEntityPK> {

}
