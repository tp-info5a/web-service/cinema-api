package com.webservice.cinema_api.controller;

import com.webservice.cinema_api.model.PersonnageEntity;
import com.webservice.cinema_api.model.PersonnageEntityPK;
import com.webservice.cinema_api.service.PersonnageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/personnage")
public class PersonnageController {

    @Autowired
    private PersonnageService personnageService;

    @GetMapping
    public List<PersonnageEntity> getPersonnages() {
        return personnageService.getPersonnages();
    }

    @PutMapping
    public ResponseEntity<PersonnageEntity> addPersonnage(@RequestBody PersonnageEntity personnageEntity) {
        PersonnageEntity entity = personnageService.addPersonnage(personnageEntity);
        return ResponseEntity.ok(entity);
    }

    @DeleteMapping
    public void deletePersonnage(@RequestBody PersonnageEntityPK entityPK) {
        personnageService.deletePersonnage(entityPK);
    }

    @PostMapping
    public ResponseEntity<PersonnageEntity> updateFilm(@RequestBody PersonnageEntity personnage) {
        PersonnageEntity personnageEntity = personnageService.updatePersonnage(personnage);
        return ResponseEntity.ok(personnageEntity);
    }

}
