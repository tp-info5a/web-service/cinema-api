package com.webservice.cinema_api.controller;

import com.webservice.cinema_api.model.ActeurEntity;
import com.webservice.cinema_api.model.FilmEntity;
import com.webservice.cinema_api.model.PersonnageEntity;
import com.webservice.cinema_api.service.ActeurService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/acteur")
public class ActeurController {

    @Autowired
    private ActeurService acteurService;

    @GetMapping
    public List<ActeurEntity> getActeurs() {
        return acteurService.getActeurs();
    }

    @GetMapping("{id}/films")
    public List<FilmEntity> getFilmsByActorId(@PathVariable int id) {
        return acteurService.getFilmsByActorId(id);
    }

    @GetMapping("{id}/personnages")
    public List<String> getRolesByActorId(@PathVariable int id) {
        return acteurService.getRolesByActorId(id);
    }

}
