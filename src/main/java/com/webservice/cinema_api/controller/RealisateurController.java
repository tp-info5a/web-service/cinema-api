package com.webservice.cinema_api.controller;

import com.webservice.cinema_api.model.FilmEntity;
import com.webservice.cinema_api.model.RealisateurEntity;
import com.webservice.cinema_api.service.RealisateurService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/realisateur")
public class RealisateurController {

    @Autowired
    private RealisateurService realisateurService;

    @GetMapping
    public List<RealisateurEntity> getRealisateurs() {
        return realisateurService.getRealisateurs();
    }

    @GetMapping("{id}/films")
    public List<FilmEntity> getFilmByReaId(@PathVariable int id) {
        return realisateurService.getFilmByIdRea(id);
    }

}
