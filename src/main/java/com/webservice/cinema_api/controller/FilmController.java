package com.webservice.cinema_api.controller;

import com.webservice.cinema_api.model.FilmEntity;
import com.webservice.cinema_api.service.FilmService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/film")
public class FilmController {

    @Autowired
    private FilmService filmService;

    @GetMapping
    public List<FilmEntity> getFilms() {
        return filmService.getFilms();
    }

    @PutMapping
    public ResponseEntity<FilmEntity> addFilm(@RequestBody FilmEntity film) {
        FilmEntity filmEntity = filmService.addFilm(film);
        return ResponseEntity.ok(filmEntity);
    }

    @DeleteMapping("/{id}")
    public void deleteFilm(@PathVariable int id) {
        filmService.deleteFilm(id);
    }

    @PostMapping
    public ResponseEntity<FilmEntity> updateFilm(@RequestBody FilmEntity film) {
        FilmEntity filmEntity = filmService.updateFilm(film);
        return ResponseEntity.ok(filmEntity);
    }

}
