package com.webservice.cinema_api.controller;

import com.webservice.cinema_api.model.CategorieEntity;
import com.webservice.cinema_api.model.FilmEntity;
import com.webservice.cinema_api.service.CategorieService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/categorie")
public class CategorieController {

    @Autowired
    private CategorieService categorieService;

    @GetMapping
    public List<CategorieEntity> getCategories() {
        return categorieService.getCategories();
    }

    @GetMapping("/{id}/films")
    public List<FilmEntity> getFilmsByCategorieId(@PathVariable String id) {
        return categorieService.getFilmsByCategorieId(id);
    }
}
